﻿using UnityEngine;
using UnityEngine.Events;
using com.alphapotato.Gameplay;
using com.faithstudio.Gameplay;

public class AimingController : MonoBehaviour
{
    #region Public Variables

    public delegate void DelegateForPassingTheValueOfAccuracy(float t_Value);
    public DelegateForPassingTheValueOfAccuracy OnPassingTheAccuracyEvaluation;

    [Header("Parameter      :   Default")]
    [Space(5.0f)]
    public Transform defaultTargetPosition;
    public Vector3 defaultOffSet;
    [Range(0f, 1f)]
    public float defaultForwardVelocity;

    [Header("Configuretion  :   ")]

    [Header("Reference      :   External")]
    public Animator horizontalCrosshairAnimator;
    public Animator verticalCrosshairAnimator;

    [Space(5.0f)]
    public Transform containerOfTarget;
    public Transform finalCrosshairPoint;

    [Space(5.0f)]
    public ParticleSystem criticalPointParticle;

    [Header("Parameter  :   AreaOfTarget")]
    [Range(0f, 0.5f)]
    public float boundaryOffset = 0.1f;
    [Range(1f, 15f)]
    public float areaOfTargetInHorizontal;
    [Range(1f, 15f)]
    public float areaOfTargetInVertical;

    [Header("Parameter  :   AreaOfCriticalPoint")]
    [Range(0f, 1f)]
    public float areaOfCriticalPointInHorizontal = 0f;
    [Range(0f, 1f)]
    public float areaOfCriticalPointInVertical = 0f;

    #endregion

    #region Private Variables

    private TimeController m_TimeManagerReference;

    private bool m_IsMovingHorizontalCrosshair;
    private bool m_IsMovingTowardsPositiveDirection;

    private float m_AbsoluteBoundary;

    private Transform m_TransformReferenceOfHorizontalCrosshair;
    private Transform m_TransformReferenceOfVerticalCrosshair;
    private Transform m_TargetPosition;
    private Vector3 m_TargetOffSet;

    private UnityAction OnTargetLocked;

    #endregion

    #region Mono Behaviour

#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(
                containerOfTarget.position,
                new Vector3(
                        (areaOfTargetInHorizontal - (areaOfTargetInHorizontal * boundaryOffset)) * 2f,
                        (areaOfTargetInVertical - (areaOfTargetInVertical * boundaryOffset)) * 2f,
                        1f
                    )
            );

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(
                containerOfTarget.position,
                new Vector3(
                        (areaOfTargetInHorizontal - (areaOfTargetInHorizontal * boundaryOffset)) * 2f * areaOfCriticalPointInHorizontal,
                        (areaOfTargetInVertical - (areaOfTargetInVertical * boundaryOffset)) * 2f * areaOfCriticalPointInVertical,
                        1f
                    )
            );
    }

#endif

    private void Awake()
    {
        m_TransformReferenceOfHorizontalCrosshair = horizontalCrosshairAnimator.transform;
        m_TransformReferenceOfVerticalCrosshair = verticalCrosshairAnimator.transform;

        horizontalCrosshairAnimator.gameObject.SetActive(false);
        verticalCrosshairAnimator.gameObject.SetActive(false);

        enabled = false;
    }


    private void Update()
    {
        Vector3 t_NewTargetContainerPosition = m_TargetPosition.position + m_TargetOffSet;
        containerOfTarget.position = t_NewTargetContainerPosition;

        Transform t_ActiveTarget = m_IsMovingHorizontalCrosshair ? m_TransformReferenceOfHorizontalCrosshair : m_TransformReferenceOfVerticalCrosshair;
        Vector3 t_TargetPosition = m_IsMovingHorizontalCrosshair ? (Vector3.right * (m_IsMovingTowardsPositiveDirection ? areaOfTargetInHorizontal : -areaOfTargetInHorizontal)) : (Vector3.up * (m_IsMovingTowardsPositiveDirection ? areaOfTargetInVertical : -areaOfTargetInVertical) + (Vector3.right * m_TransformReferenceOfHorizontalCrosshair.localPosition.x));
        Vector3 t_ModifiedPosition = Vector3.Lerp(
                t_ActiveTarget.localPosition,
                t_TargetPosition,
                defaultForwardVelocity * m_TimeManagerReference.GetAbsoluteDeltaTime()
            );

        if (Vector3.Distance(t_ModifiedPosition, t_TargetPosition) <= m_AbsoluteBoundary)
        {

            m_IsMovingTowardsPositiveDirection = !m_IsMovingTowardsPositiveDirection;
        }

        t_ActiveTarget.localPosition = t_ModifiedPosition;
    }

    #endregion

    #region Configuretion

    private void SetCriticalPoint()
    {
        float t_HorizontalArea = (areaOfTargetInHorizontal - (areaOfTargetInHorizontal * boundaryOffset)) * areaOfCriticalPointInHorizontal;
        float t_VerticalArea = (areaOfTargetInVertical - (areaOfTargetInVertical * boundaryOffset)) * areaOfCriticalPointInVertical;
        criticalPointParticle.transform.localPosition = new Vector3(
                        Random.Range(-t_HorizontalArea, t_HorizontalArea),
                        Random.Range(-t_VerticalArea, t_VerticalArea),
                        0f
                    );
        criticalPointParticle.Play();
    }

    private float GetEvaluatedValueFromAccuracy()
    {

        float t_HorizontalArea = (areaOfTargetInHorizontal - (areaOfTargetInHorizontal * boundaryOffset)) * areaOfCriticalPointInHorizontal;
        float t_VerticalArea = (areaOfTargetInVertical - (areaOfTargetInVertical * boundaryOffset)) * areaOfCriticalPointInVertical;

        float t_EvaluationOnHorizontalAim   = 1f - Mathf.Clamp01(Mathf.Abs(criticalPointParticle.transform.localPosition.x - m_TransformReferenceOfHorizontalCrosshair.localPosition.x) / t_HorizontalArea); //1f - Mathf.Abs(m_TransformReferenceOfHorizontalCrosshair.localPosition.x / t_HorizontalArea);
        float t_EvaluationOnVerticalAim     = 1f - Mathf.Clamp01(Mathf.Abs(criticalPointParticle.transform.localPosition.y - m_TransformReferenceOfVerticalCrosshair.localPosition.y) / t_VerticalArea);//1f - Mathf.Abs(m_TransformReferenceOfVerticalCrosshair.localPosition.y / t_VerticalArea);

        //Debug.Log(t_HorizontalArea + " -> " + t_EvaluationOnHorizontalAim + ", " + t_VerticalArea + " -> " + t_EvaluationOnVerticalAim);

        return (t_EvaluationOnHorizontalAim + t_EvaluationOnVerticalAim) / 2.0f;
    }

    private void OnTouchDown(Vector3 t_TouchPosition)
    {
        if (m_IsMovingHorizontalCrosshair)
        {

            horizontalCrosshairAnimator.gameObject.SetActive(false);
            verticalCrosshairAnimator.gameObject.SetActive(true);
            verticalCrosshairAnimator.SetTrigger("APPEAR");


            m_IsMovingTowardsPositiveDirection = true;
            m_IsMovingHorizontalCrosshair = false;
        }
        else
        {

            criticalPointParticle.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            Vector3 t_ModifiedPosition = new Vector3(
                    m_TransformReferenceOfHorizontalCrosshair.position.x,
                    m_TransformReferenceOfVerticalCrosshair.position.y,
                    finalCrosshairPoint.position.z
                );
            finalCrosshairPoint.position = t_ModifiedPosition;
            

            Vector3 t_LocalPosition = finalCrosshairPoint.localPosition;
            t_LocalPosition.z = 0;
            finalCrosshairPoint.localPosition = t_LocalPosition;

            verticalCrosshairAnimator.gameObject.SetActive(false);

            GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;

            OnPassingTheAccuracyEvaluation?.Invoke(GetEvaluatedValueFromAccuracy());
            OnTargetLocked?.Invoke();

            enabled = false;
        }
    }

    #endregion

    #region Public Callback

    public void PreProcess(
        UnityAction OnTargetLocked = null,
        Transform t_TargetPosition = null,
        Vector3 t_TargetOffset = new Vector3())
    {

        m_TimeManagerReference = TimeController.Instance;
        m_AbsoluteBoundary = ((areaOfTargetInHorizontal + areaOfTargetInVertical) / 2.0f) * boundaryOffset;

        this.OnTargetLocked = OnTargetLocked;

        if (t_TargetPosition == null)
            m_TargetPosition = defaultTargetPosition;
        else
            m_TargetPosition = t_TargetPosition;

        if (t_TargetOffset == new Vector3())
            m_TargetOffSet = defaultOffSet;
        else
            m_TargetOffSet = t_TargetOffset;

        m_IsMovingHorizontalCrosshair = true;
        m_IsMovingTowardsPositiveDirection = true;

        m_TransformReferenceOfHorizontalCrosshair.localPosition = Vector3.left * (areaOfTargetInHorizontal - m_AbsoluteBoundary);
        m_TransformReferenceOfVerticalCrosshair.localPosition = Vector3.up * (areaOfTargetInVertical - m_AbsoluteBoundary);

        SetCriticalPoint();

        horizontalCrosshairAnimator.gameObject.SetActive(true);
        horizontalCrosshairAnimator.SetTrigger("APPEAR");

        if (!enabled)
        {

            GlobalTouchController.Instance.OnTouchDown += OnTouchDown;

            enabled = true;
        }
    }

    public void PostProcess()
    {
        enabled = false;
        GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
        OnPassingTheAccuracyEvaluation?.Invoke(0);

        horizontalCrosshairAnimator.gameObject.SetActive(false);
        verticalCrosshairAnimator.gameObject.SetActive(false);
        criticalPointParticle.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
    }



    #endregion
}
