﻿namespace com.alphapotato.Gameplay {
    
    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(GPSTrackerController))]
    public class GPSTrackerControllerEditor : Editor {
        private GPSTrackerController Reference;

        private void OnEnable() {
            
            Reference = (GPSTrackerController) target;
        }

        public override void OnInspectorGUI(){

            serializedObject.Update();

            base.OnInspectorGUI();

            serializedObject.ApplyModifiedProperties();
        }

        private void CustomGUI(){

            if(GUILayout.Button("Find Path")){

                int t_NumberOfGuidedPath = Reference.listOfGuidedPath.Count;
                for(int i = 0 ; i < t_NumberOfGuidedPath; i++){

                    if(Reference.listOfGuidedPath[i].editorPathContainer != null){

                        Reference.listOfGuidedPath[i].arrayOfPath = Reference.listOfGuidedPath[i].editorPathContainer.GetComponentsInChildren<Transform>();
                    }
                }
            }
        }
    }
}