﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public class PackageManagerEditorWindow : EditorWindow
{

    #region CustomVariables

    public struct PackageInfo
    {
        public string packageLink;
        public string packageVersion;
    }

    #endregion

    #region Private Variables

    private string m_NameOfManifestDirectory  = "Packages";
    private string m_PackageName;
    private string m_RepositoryLink;

    #endregion

    #region Public Callback :   Static

    [MenuItem("Window/FAITH Studio/PackageManager (FAITH)")]
    public static void ShowWindow(){

        GetWindow<PackageManagerEditorWindow>("PackageManager (FAITH)");
    }

    public static void HideWindow(){

    }

    #endregion

    #region EditorWindow

    private void OnGUI() {
        
        m_PackageName    = EditorGUILayout.TextField("PackageName",m_PackageName);
        m_RepositoryLink = EditorGUILayout.TextField("RepositoryLink", m_RepositoryLink);
        if(GUILayout.Button("Show Directory")){
            
            AddNewRepositoryToManifestJSON(m_PackageName,m_RepositoryLink);
        }
    }

    #endregion

    #region Configuretion   :   Reading/Writing manifest.json

    private void AddNewRepositoryToManifestJSON(string t_PackageName, string t_RepositoryLink){

        string t_StreamingAssetPath = Application.streamingAssetsPath;
        string[] t_Split = t_StreamingAssetPath.Split('/');
        string t_ManifestPath = "";
        
        int t_NumberOfSplit = t_Split.Length - 2;
        for(int i = 0 ; i < t_NumberOfSplit; i++){

            t_ManifestPath += t_Split[i];
            t_ManifestPath += "/";
        }
        t_ManifestPath += m_NameOfManifestDirectory;
        t_ManifestPath += "/";
        t_ManifestPath += "manifest.json";//"manifest.json"; 

        string t_Result         = System.IO.File.ReadAllText(t_ManifestPath);

        //Extracting    :   Package
        string[] t_SplitByComa  = t_Result.Split(',');
        t_NumberOfSplit         = t_SplitByComa.Length;
        List<PackageInfo> t_CurrentPackageInfo = new List<PackageInfo>();

        for(int i = 0; i < t_NumberOfSplit; i++){

            string t_ConcatinatedString = "";
            List<char> t_Converted      = t_SplitByComa[i].ToList();
            int t_NumberOfCharacter     = t_Converted.Count;
            for(int j =0; j < t_NumberOfCharacter; ){

                if(t_Converted[j] == ' ' 
                || t_Converted[j] == '{' 
                || t_Converted[j] == '}'
                || t_Converted[j] == '\t'
                || t_Converted[j] == '\n'){

                    t_Converted.RemoveAt(j);
                    t_Converted.TrimExcess();

                    t_NumberOfCharacter--;
                }else{

                    t_ConcatinatedString += t_Converted[j];
                    j++;
                }
            }

            string[] t_SplitByColon = t_ConcatinatedString.Split(':');
            if(i == 0){
                t_CurrentPackageInfo.Add(new PackageInfo(){
                    packageLink = t_SplitByColon[1],
                    packageVersion = t_SplitByColon[2]
                });
            }else{
                t_CurrentPackageInfo.Add(new PackageInfo(){
                    packageLink = t_SplitByColon[0],
                    packageVersion = t_SplitByColon[1]
                });
            }

            Debug.Log(t_CurrentPackageInfo[t_CurrentPackageInfo.Count - 1].packageLink + " : " + t_CurrentPackageInfo[t_CurrentPackageInfo.Count - 1].packageVersion);
        }
        
        //WritingPackage
        using (StreamWriter streamWrite = new StreamWriter(t_ManifestPath)){
            
            bool t_IsRepositoryAlreadyAdded = false;

            streamWrite.WriteLine("{");
            streamWrite.WriteLine("\t\"dependencies\":{");

            int t_NumberOfPackage = t_CurrentPackageInfo.Count;
            for(int i = 0 ; i < t_NumberOfPackage; i++){

                if(t_CurrentPackageInfo[i].packageLink  == t_PackageName)
                    t_IsRepositoryAlreadyAdded = true;

                streamWrite.WriteLine(
                    "\t\t"
                    + t_CurrentPackageInfo[i].packageLink 
                    + " : " 
                    + t_CurrentPackageInfo[i].packageVersion
                    + ((i == (t_NumberOfPackage - 1)) ? (t_IsRepositoryAlreadyAdded ? "" : ",") : ","));
            }

            if(!t_IsRepositoryAlreadyAdded){

                streamWrite.WriteLine(
                    "\t\t"
                    + "\""
                    + t_PackageName
                    + "\""
                    + " : " 
                    + "\"git+"
                    + t_RepositoryLink
                    + "\"");
            }    

            streamWrite.WriteLine("\t}");
            streamWrite.WriteLine("}");
        }
        AssetDatabase.Refresh();
    }

    #endregion
}
