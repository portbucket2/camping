﻿namespace com.alphapotato.GameplayService {
    using UnityEditor;
    using UnityEngine;

    [CustomEditor (typeof (FetchRenderSettings))]
    public class FetchRenderSettingsEditor : Editor {

        private FetchRenderSettings Reference;

        private void OnEnable () {

            if (target == null)
                return;

            Reference = (FetchRenderSettings) target;
            Reference.SyncRenderSettings();
        }

        public override void OnInspectorGUI () {

            serializedObject.Update ();

            if(GUILayout.Button("Fetch Data of RenderSettings")){

                Reference.SyncRenderSettings();
            }

            DrawHorizontalLine();

            DrawDefaultInspector ();

            serializedObject.ApplyModifiedProperties ();
        }

        #region Editor Moduler Function

        private void DrawHorizontalLine () {

            EditorGUILayout.LabelField ("", GUI.skin.horizontalSlider);
        }

        private void DrawSettingsEditor (Object settings, System.Action OnSettingsUpdated, ref bool foldout, ref Editor editor) {

            if (settings != null) {

                using (var check = new EditorGUI.ChangeCheckScope ()) {

                    foldout = EditorGUILayout.InspectorTitlebar (foldout, settings);

                    if (foldout) {

                        CreateCachedEditor (settings, null, ref editor);
                        editor.OnInspectorGUI ();

                        if (check.changed) {

                            if (OnSettingsUpdated != null) {

                                OnSettingsUpdated.Invoke ();
                            }
                        }
                    }
                }
            }
        }

        #endregion

    }
}