﻿using UnityEngine;
using com.faithstudio.Camera;
using com.alphapotato.GameplayService;
using System.Collections;
using System.Collections.Generic;
public class CinematicsController : MonoBehaviour
{

    #region Custom Variables

    [System.Serializable]
    public class CinematicTransation
    {
        public float cameraVelocity;
        public AnimationCurve curveForCameraVelocity;

        [Space(5.0f)]
        public float cameraAngulerVelocity;
        public AnimationCurve curveForCameraAngulerVelocity;

        [Space(5.0f)]
        public Transform transationPoint;
        public Transform focusPoint;
    }

    [System.Serializable]
    public class CinematicCutScene
    {
        public bool isLoop;

        public CinematicTransation[] cinematicTransations;
    }

    #endregion

    #region Public Variables

    public AdditiveSceneManager additiveSceneManagerReference;
    public CinematicCutScene[] cinematicCutScenes;

    #endregion

    #region Private Variables

    private bool IsCinematicCutSceneEnabled;

    #endregion

    #region Mono Behaviour

    private void Awake(){

        additiveSceneManagerReference.LoadScene(0);
    }

    private void Start() {
        ShowCinematicCutScene(0);
    }

    #endregion

    #region Configuretion

    private bool IsValidCutSceneIndex(int t_CutSceneIndex){

        if(t_CutSceneIndex >= 0 && t_CutSceneIndex < cinematicCutScenes.Length)
            return true;

        return false;
    }

    private IEnumerator ControllerForShowingCutScene(int t_CutSceneIndex){

        WaitForSeconds t_CycleDelay = new WaitForSeconds(1f);

        int t_NumberOfTransation = cinematicCutScenes[t_CutSceneIndex].cinematicTransations.Length;
        bool t_IsTransationEnd;
        for(int i = 0 ; i < t_NumberOfTransation; i++){

            t_IsTransationEnd = false;
            CameraMovementController.Instance.FocusCameraWithOrigin(
                cinematicCutScenes[t_CutSceneIndex].cinematicTransations[i].transationPoint,
                new List<Transform>() {cinematicCutScenes[t_CutSceneIndex].cinematicTransations[i].focusPoint},
                true,
                Vector3.zero,
                Vector3.zero,
                0,
                cinematicCutScenes[t_CutSceneIndex].cinematicTransations[i].cameraVelocity,
                cinematicCutScenes[t_CutSceneIndex].cinematicTransations[i].cameraAngulerVelocity,
                cinematicCutScenes[t_CutSceneIndex].cinematicTransations[i].curveForCameraAngulerVelocity,
                delegate{
                    t_IsTransationEnd = true;
                }
            );
            
            WaitUntil t_WaitUntilTheTransationEnd = new WaitUntil(() => {
                if(!t_IsTransationEnd)
                    return false;
                
                return true;
            });

            yield return t_WaitUntilTheTransationEnd;

            if(cinematicCutScenes[t_CutSceneIndex].isLoop && i == (t_NumberOfTransation - 1))
                i = 0;
        }

        StopCoroutine(ControllerForShowingCutScene(0));
    }

    #endregion

    #region Public Callback

    public void ShowCinematicCutScene(int t_CutSceneIndex){

        if(IsValidCutSceneIndex(t_CutSceneIndex) && !IsCinematicCutSceneEnabled){

            IsCinematicCutSceneEnabled = true;
            StartCoroutine(ControllerForShowingCutScene(t_CutSceneIndex));
        }
    }

    #endregion
}
