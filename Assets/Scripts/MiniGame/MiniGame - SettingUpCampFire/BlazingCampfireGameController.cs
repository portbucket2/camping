﻿using System.Collections;
using System.Collections.Generic;
using com.alphapotato.Gameplay;
using com.faithstudio.Camera;
using com.faithstudio.ScriptedParticle;
using com.faithstudio.UI;
using UnityEngine;

public class BlazingCampfireGameController : MiniGameController {
    #region Public Variables

    [Header ("Parameter  :   Camera")]
    [Range (1f, 5f)]
    public float durationOfDisplayBeforeSettingFire = 1f;
    public Transform cameraOriginOnFirePlaceBeforeSettingFire;
    public List<Transform> focalPointOnFirePlaceBeforeSettingFire;

    [Space (5.0f)]
    [Range (1f, 5f)]
    public float durationOfDisplayAfterSettingFire = 1;
    public Transform cameraOriginOnFirePlaceAfterSettingFire;
    public List<Transform> focalPointOnFirePlaceAfterSettingFire;

    [Header ("Parameter  :   SettingFire")]
    public ItemFactory fireKnife;
    [Range (0f, 1f)]
    public float positionOffset;
    [Range(1f,1000f)]
    public float fireplaceHealth = 100f;
    [Range(0f,0.01f)]
    public float rateOfRegainingFireHealth;
    [Range(0f,0.25f)]
    public float threshHoldOnSettingFire;
    public ParticleController fireplaceParticle;

    #endregion

    #region Private Variables

    private UIProgressBar UIProgressBarForSettingFire;
    private Transform m_TransformReferenceOfSelectedFireComponent;
    
    private bool m_IsFireRegainingControllerRunning;
    private float m_CurrentHealthOfFireplace;

    #endregion

    #region Mono Behaviour

    private void Awake () {

        base.OnMiniGameStart += PreProcess;
        
    }

    #endregion

    #region Configuretion   :   Touch

    private void OnTouchDown (RaycastHit t_RaycastHit) {

        m_IsFireRegainingControllerRunning = false;
        PositioningTheFireComponent (t_RaycastHit,false,true);
    }

    private void OnTouch (RaycastHit t_RaycastHit) {

        PositioningTheFireComponent (t_RaycastHit, true);
    }

    private void OnTouchUp (RaycastHit t_RaycastHit) {

        if(!m_IsFireRegainingControllerRunning){

            m_IsFireRegainingControllerRunning = true;
            StartCoroutine(ControllerForRegainingFireHealth());
        }
    }

    #endregion

    #region Configuretion

    private void PreProcess () {

        UIProgressBarForSettingFire = UIStateController.Instnace.UIProgressbarReference;
        m_TransformReferenceOfSelectedFireComponent = fireKnife.GetSelectedItemMesh ().transform;
        
        m_CurrentHealthOfFireplace = fireplaceHealth;
        
        base.OnInteractWithTheObjectOnTouchDown += OnTouchDown;
        base.OnInteractWithTheObjectOnTouch += OnTouch;
        base.OnInteractWithTheObjectOnTouchUp += OnTouchUp;

        StartCoroutine (ControllerForPreProcess ());
    }

    private void PostProcess(){

        base.OnInteractWithTheObjectOnTouchDown -= OnTouchDown;
        base.OnInteractWithTheObjectOnTouch -= OnTouch;
        base.OnInteractWithTheObjectOnTouchUp -= OnTouchUp;
    }

    private IEnumerator ControllerForPreProcess () {

        //Set   :   Camera
        bool t_WaitUntilCameraReachAtDestination = false;
        WaitUntil t_WaitUntilCameraReach = new WaitUntil (() => {

            if (!t_WaitUntilCameraReachAtDestination)
                return false;

            return true;
        });

        t_WaitUntilCameraReachAtDestination = false;
        CameraMovementController.Instance.FocusCameraWithOrigin (
            cameraOriginOnFirePlaceBeforeSettingFire,
            focalPointOnFirePlaceBeforeSettingFire,
            delegate {
                t_WaitUntilCameraReachAtDestination = true;
            }
        );

        yield return t_WaitUntilCameraReach;
        yield return new WaitForSeconds (durationOfDisplayBeforeSettingFire);

        //Selecting Fire Componenet
        UIProgressBarForSettingFire.ShowProgressbar();
        WaitUntil t_WaitUntilTheFullFireIsSet = new WaitUntil(() => {

            if(m_CurrentHealthOfFireplace > 0)
                return false;

            return true;
        });
        yield return t_WaitUntilTheFullFireIsSet;
        //Reset :   CameraPosition

        t_WaitUntilCameraReachAtDestination = false;
        CameraMovementController.Instance.FocusCameraWithOrigin (
            cameraOriginOnFirePlaceAfterSettingFire,
            focalPointOnFirePlaceAfterSettingFire,
            delegate {
                t_WaitUntilCameraReachAtDestination = true;
            }
        );

        yield return t_WaitUntilCameraReach;

        PostProcess();

        yield return new WaitForSeconds (durationOfDisplayAfterSettingFire);
    }

    private IEnumerator ControllerForRegainingFireHealth(){

        float t_CycleLength = 0.033f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        float t_HealthThreshold = fireplaceHealth * 0.01f;
        while(m_IsFireRegainingControllerRunning){

            m_CurrentHealthOfFireplace = Mathf.Lerp(m_CurrentHealthOfFireplace, fireplaceHealth, rateOfRegainingFireHealth);
            if(m_CurrentHealthOfFireplace >= t_HealthThreshold)
                m_CurrentHealthOfFireplace = fireplaceHealth;
            
            float t_Progression = m_CurrentHealthOfFireplace / fireplaceHealth;

            UIProgressBarForSettingFire.UpdateProgressBar(1f - t_Progression);
            fireplaceParticle.LerpOnDefault(t_Progression);

            if(t_Progression == 1){
                fireplaceParticle.StopParticle(false);
                m_IsFireRegainingControllerRunning = false;
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForRegainingFireHealth());
    }

    private void PositioningTheFireComponent (RaycastHit t_RaycastHit,bool t_ReduceHealthOfFirePlace, bool t_SnapOnPosition = false) {

        if (m_TransformReferenceOfSelectedFireComponent != null) {

            Vector3 t_ObjectPosition = t_RaycastHit.transform.position;
            Vector3 t_OnHitPosition = t_RaycastHit.point;

            Vector3 t_ModifedPosition;
            Quaternion t_ModifiedRotation;
            
            if (t_SnapOnPosition) {

                t_ModifedPosition = t_OnHitPosition + Vector3.Normalize (t_OnHitPosition - t_ObjectPosition) * positionOffset;
                t_ModifiedRotation =  Quaternion.LookRotation (t_ObjectPosition - t_RaycastHit.normal);
            } else {

                t_ModifedPosition = Vector3.Lerp (
                    m_TransformReferenceOfSelectedFireComponent.position,
                    t_OnHitPosition + Vector3.Normalize (t_OnHitPosition - t_ObjectPosition) * positionOffset,
                    0.2f
                );
                t_ModifiedRotation = Quaternion.Slerp (
                    m_TransformReferenceOfSelectedFireComponent.rotation,
                    Quaternion.LookRotation (t_ObjectPosition - t_RaycastHit.normal),
                    0.2f
                );
            }

            m_TransformReferenceOfSelectedFireComponent.position = t_ModifedPosition;
            m_TransformReferenceOfSelectedFireComponent.rotation = t_ModifiedRotation;

            if(t_ReduceHealthOfFirePlace){

                m_CurrentHealthOfFireplace -= fireKnife.GetCurrentStateOfSelectedItem();
                float t_Progression = 1f - (m_CurrentHealthOfFireplace / fireplaceHealth);

                if(t_Progression >= threshHoldOnSettingFire){

                    fireplaceParticle.PlayParticle();
                }

                fireplaceParticle.LerpOnPreset(t_Progression);
                UIProgressBarForSettingFire.UpdateProgressBar(t_Progression);
            }
        }

    }

    #endregion

    #region Public Callback

    #endregion
}