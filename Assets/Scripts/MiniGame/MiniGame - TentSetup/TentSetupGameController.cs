﻿using System.Collections;
using System.Collections.Generic;
using com.alphapotato.Gameplay;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;

public class TentSetupGameController : MiniGameController {

    #region Custom Variables

    [System.Serializable]
    public class TentNail {

        #region Public Variables

        [Header ("Parameter  :   Camera")]
        public Transform cameraOriginOnTentNail;
        public List<Transform> focalPointOnNail;

        [Header ("Parameter  :   TentNail")]
        public Transform tentNailReference;
        public TentNailAttribute tentNailAttributeReference;
        [Range (1f, 100)]
        public float initialRequiredForceToComplete = 1;
        [Range (0.01f, 5f)]
        public float nailDepth = 0.01f;
        public AnimationCurve nailDepthThroughForce = new AnimationCurve (new Keyframe[] { new Keyframe (0f, 0.1f), new Keyframe (1f, 1f) });

        #endregion

        #region Private Variables

        private Vector3 m_InitialLocalPositionOfNail;
        private bool m_IsHammeringOnNailAllowed;
        private float m_RequiredForceToComplete;
        private float m_HammerPower;

        private UnityAction<RaycastHit> OnPlacingHammerOverTheNail;
        private UnityAction<float> OnNailGettingHammered;
        private UnityAction OnNailGrounded;

        #endregion

        #region Public Callback

        public void Initialization (UnityAction<RaycastHit> OnPlacingHammerOverTheNail) {

            this.OnPlacingHammerOverTheNail = OnPlacingHammerOverTheNail;

            m_IsHammeringOnNailAllowed = false;
            m_InitialLocalPositionOfNail = tentNailReference.transform.localPosition;
            tentNailAttributeReference.PreProcess (EnforceOnNail);
        }

        public void PreProcess (float t_HammerPower, UnityAction OnNailGrounded = null) {

            PreProcess (t_HammerPower, null, OnNailGrounded);
        }

        public void PreProcess (float t_HammerPower, UnityAction<float> OnNailGettingHammered, UnityAction OnNailGrounded = null) {

            //tentNailReference.ShowAppearAnimation();

            m_RequiredForceToComplete = initialRequiredForceToComplete;
            tentNailReference.transform.localPosition = m_InitialLocalPositionOfNail;

            m_HammerPower = t_HammerPower;
            this.OnNailGettingHammered = OnNailGettingHammered;
            this.OnNailGrounded = OnNailGrounded;

            m_IsHammeringOnNailAllowed = true;

        }

        public void EnforceOnNail (RaycastHit t_RaycastHit) {

            if (m_IsHammeringOnNailAllowed) {

                OnPlacingHammerOverTheNail.Invoke (t_RaycastHit);

                m_RequiredForceToComplete -= m_HammerPower;
                m_RequiredForceToComplete = m_RequiredForceToComplete <= 0 ? 0 : m_RequiredForceToComplete;

                float t_Progression = 1f - (m_RequiredForceToComplete / initialRequiredForceToComplete);
                
                UIStateController.Instnace.UIProgressbarReference.UpdateProgressBar(t_Progression);
                
                Vector3 t_ModifiedPosition = m_InitialLocalPositionOfNail +
                    ((tentNailReference.transform.up * -1f) * nailDepth * nailDepthThroughForce.Evaluate (t_Progression));
                tentNailReference.transform.localPosition = t_ModifiedPosition;

                if (OnNailGettingHammered != null)
                    OnNailGettingHammered.Invoke (t_Progression);

                if (m_RequiredForceToComplete == 0 && OnNailGrounded != null) {
                    OnNailGrounded.Invoke ();
                    m_IsHammeringOnNailAllowed = false;
                }
            }

        }

        public void SetRobeOfTentNail(){

            tentNailAttributeReference.SetRobeOfTentNail();
        }

        #endregion

    }

    #endregion

    #region Public Variables

    [Header ("Parameter  :   Camera")]
    [Range (1f, 5f)]
    public float durationOfDisplayBeforeBuild = 1f;
    public Transform cameraOriginOnTentBeforeBuild;
    public List<Transform> focalPointOnTentBeforeBuild;

    [Space (5.0f)]
    [Range (1f, 5f)]
    public float durationOfDisplayAfterBuild = 1;
    public Transform cameraOriginOnTentAfterBuild;
    public List<Transform> focalPointOnTentAfterBuild;

    [Header ("Parameter  :   Tent")]
    public Animator tentAnimator;
    [Header ("Parameter  :   TentNail")]
    [Range(0.01f,5f)]
    public float hammerOffsetOnNail = 0.01f;
    public TentNail[] tentNails;

    [Header ("Parameter  :   Item")]
    public ItemFactory tentHammer;

    #endregion

    #region  Private Variables

    #endregion

    #region Mono Behaviour

    private void Awake () {


        base.OnMiniGameStart += PreProcess;
        base.OnInteractWithTheObjectOnTouchDown = OnInteractedWithTentNail;

        int t_NumberOfNail = tentNails.Length;
        for (int i = 0; i < t_NumberOfNail; i++) {

            tentNails[i].Initialization (OnPlacingHammerOverTheNail);
        }
    }

    #endregion

    #region Configuretion

    private void PreProcess () {

        StartCoroutine (ControllerForPreProcess ());
    }

    private IEnumerator ControllerForPreProcess () {

        //Set   :   Camera
        bool t_IsCameraReachedToTentPosition = false;

        CameraMovementController.Instance.FocusCameraWithOrigin (
            cameraOriginOnTentBeforeBuild,
            focalPointOnTentBeforeBuild,
            delegate {
                t_IsCameraReachedToTentPosition = true;
            }
        );

        WaitUntil t_WaitUntilCameraReach = new WaitUntil (() => {

            if (!t_IsCameraReachedToTentPosition)
                return false;

            return true;
        });
        yield return t_WaitUntilCameraReach;
        yield return new WaitForSeconds (durationOfDisplayBeforeBuild);

        //Set   :   Cojecutive Camera Angle For Hammering Tent Nail
        bool t_IsNailGrounded = false;
        WaitUntil t_WaitUntilTheNailGrounded = new WaitUntil (() => {

            if (!t_IsNailGrounded)
                return false;

            return true;
        });

        int t_NumberOfNail = tentNails.Length;
        for (int i = 0; i < t_NumberOfNail; i++) {
            
            List<Transform> t_FocalPoints = new List<Transform>(focalPointOnTentBeforeBuild);
            t_FocalPoints.AddRange(tentNails[i].focalPointOnNail);
            CameraMovementController.Instance.FocusCameraWithOrigin (
                tentNails[i].cameraOriginOnTentNail,
                t_FocalPoints, 
                delegate {
                    
                    UIStateController.Instnace.UIProgressbarReference.ShowProgressbar();
                    UIStateController.Instnace.UIProgressbarReference.UpdateProgressBar(0f);

                    tentNails[i].PreProcess (
                        tentHammer.GetCurrentStateOfSelectedItem (),
                        delegate {

                            t_IsNailGrounded = true;
                        });
                }
            );

            t_IsNailGrounded = false;
            yield return t_WaitUntilTheNailGrounded;

            yield return new WaitForSeconds(1f);
        }

        //Reset :   CameraPosition
        t_IsCameraReachedToTentPosition = false;

        CameraMovementController.Instance.FocusCameraWithOrigin (
            cameraOriginOnTentAfterBuild,
             focalPointOnTentAfterBuild,
            delegate {
                t_IsCameraReachedToTentPosition = true;
                tentAnimator.SetTrigger("APPEAR");
            }
        );

        yield return t_WaitUntilCameraReach;

        for (int i = 0; i < t_NumberOfNail; i++){

            tentNails[i].SetRobeOfTentNail();
        }

        yield return new WaitForSeconds (durationOfDisplayAfterBuild);

        StopCoroutine (ControllerForPreProcess ());

        base.EndGame();
    }

    private void OnInteractedWithTentNail (RaycastHit t_RaycastHit) {

        t_RaycastHit.collider.GetComponent<TentNailAttribute> ().HammerOnNail (t_RaycastHit);
    }

    private void OnPlacingHammerOverTheNail (RaycastHit t_RaycastHit) {

        Transform t_TentNailTransform = t_RaycastHit.collider.GetComponent<TentNailAttribute>().GetTransformReferenceOfTentNail();

        Transform t_TransformReferenceForSelectedHammer = tentHammer.GetSelectedItemMesh ().transform;
        Vector3 t_HammeringPosition = t_TentNailTransform.position + (t_TentNailTransform.up * hammerOffsetOnNail);
        
        t_TransformReferenceForSelectedHammer.position = t_HammeringPosition;
        t_TransformReferenceForSelectedHammer.rotation = Quaternion.LookRotation (t_TentNailTransform.position - t_TransformReferenceForSelectedHammer.position);

        HammerAttribute t_HammerAttributeReference = tentHammer.GetSelectedItemMesh().GetComponent<HammerAttribute>();
        t_HammerAttributeReference.ShowHammingAnimation();
        t_HammerAttributeReference.ShowHammeringParticle(t_HammeringPosition);
    }

    #endregion
}