﻿using UnityEngine;

public class HammerAttribute : MonoBehaviour
{
    #region Public Variables

    public Animator         hammerAnimator;
    public ParticleSystem   hammeringParticle;

    #endregion

    #region Private Variables

    private Vector3 m_InitialLocalPositionOfHammeringParticle;

    #endregion

    #region  Mono Behaviour

    private void Awake() {
        
        m_InitialLocalPositionOfHammeringParticle = hammeringParticle.transform.localPosition;
    }

    #endregion

    #region Public Callback

    public void ShowHammingAnimation(){

        hammerAnimator.SetTrigger("HAM");
    }

    public void ShowHammeringParticle(Vector3 t_HammeringParticlePosition = new Vector3()){

        if(t_HammeringParticlePosition != Vector3.zero)
            hammeringParticle.transform.position = t_HammeringParticlePosition;
        else
            hammeringParticle.transform.position = m_InitialLocalPositionOfHammeringParticle;

        hammeringParticle.Play();
    }

    #endregion
}
