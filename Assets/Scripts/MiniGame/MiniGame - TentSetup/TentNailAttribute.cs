﻿using UnityEngine;
using UnityEngine.Events;

public class TentNailAttribute : MonoBehaviour
{
    #region Public Variables

    public Animator animatorReferenceOfTentNail;
    public Animator animatorReferenceOfRopeOfTentNail;

    #endregion

    #region Private Variables

    private UnityAction<RaycastHit> OnNailHammered;

    private Vector3 m_InitialLocalPositionOfTentNail;
    private Vector3 m_InitialLocalPositionOfRopeOfTentNail;

    #endregion

    #region Mono Behaviour

    private void Awake(){

        m_InitialLocalPositionOfTentNail = animatorReferenceOfTentNail.transform.localPosition;
        m_InitialLocalPositionOfRopeOfTentNail = animatorReferenceOfRopeOfTentNail.transform.localPosition;
        
    }

    #endregion

    #region Configuretion

    #endregion

    #region Public Callback

    public void PreProcess(UnityAction<RaycastHit> OnNailHammered){

        this.OnNailHammered = OnNailHammered;

        animatorReferenceOfTentNail.transform.localPosition         = m_InitialLocalPositionOfTentNail;
        //animatorReferenceOfRopeOfTentNail.transform.localPosition   = m_InitialLocalPositionOfRopeOfTentNail;
    }

    public void PostProcess(){

        
    }

    public Transform GetTransformReferenceOfTentNail(){

        return animatorReferenceOfTentNail.transform;
    }

    public void HammerOnNail(RaycastHit t_RaycastHit){
        
        animatorReferenceOfTentNail.SetTrigger("HAMMERED");
        OnNailHammered.Invoke(t_RaycastHit);
    }

    public void ShowAppearAnimation(){

        animatorReferenceOfTentNail.SetTrigger("APPEAR");
        animatorReferenceOfRopeOfTentNail.SetTrigger("APPEAR");
    }

    public void ShowDisappearAnimation(){

        animatorReferenceOfTentNail.SetTrigger("DISAPPEAR");
        animatorReferenceOfRopeOfTentNail.SetTrigger("DISAPPEAR");
    }

    public void SetRobeOfTentNail(){

        animatorReferenceOfRopeOfTentNail.SetTrigger("GROUNDED");
    }

    #endregion
}
